# How to live stream with mobile device

## Host Computer
- Install OBS: https://obsproject.com/
- Install (Mac) BlackHole (to stream audio from meeting)
    - Brew Installer: https://brew.sh/
    - BlackHole (pick option 2): https://github.com/ExistentialAudio/BlackHole#option-2-install-via-homebrew
- (Mac) Audi Midi Setup: https://guusbaggermans.medium.com/how-to-record-any-meeting-on-your-mac-using-only-free-software-7db340f5db45
- Create YouTube account

## Video and Audio Source
- Mobile device (and stabilizer if possible)
- WebCam (connect to Mac)

## Aggregation
- Register a Google account for Google Meet
    - https://apps.google.com/meet/pricing/
    - 1:1 max 24 hours

## Onshow
1. Mac start Google Meet meeting
2. Mac webcam point to steady position and receive audio
3. Phone join Google Meet meeting
4. Phone filming on moving position and stay close for receiving audio
5. Mac control OBS to show PPT or Google Meet meeting screen

## References
Other tested options:
- MS Teams: https://www.microsoft.com/en-ww/microsoft-teams/compare-microsoft-teams-options?market=hk&activetab=pivot:subheadfeatureregion
    - max 60 min only
- OBS Camera: https://obs.camera/docs/getting-started/ios-camera-plugin-usb/
    - need try with USB, wifi doesnt work, with watermark
- Epoccam: https://www.elgato.com/en/epoccam
    - video only with free version, with tiny watermark
